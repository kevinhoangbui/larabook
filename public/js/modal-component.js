// register modal component
Vue.component('modal', {
  template: '#modal-template',
  props: {
    show: {
      type: Boolean,
      required: true,
      twoWay: true
    },
    liked_users: {
    	required: true,
    	twoWay: true
    }
  }
});