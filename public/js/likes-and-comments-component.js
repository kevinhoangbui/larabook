Vue.component('likes-and-comments', {
	template: '#likes-and-comments-template',
	data: function(){
		return {
			likeImagePath: 'images/icons/like.jpg',
			likedImagePath: 'images/icons/liked.jpg',
			commentImagePath: 'images/icons/comment.jpg',
			commentsImagePath: 'images/icons/comments.jpg',
			likeText: 'Like',
			unlikeText: 'Unlike'
		}	
	},
	//this component receive 8 properties
	//status_id: the status_id property is used to save one more like to the database via an ajax request
	//comment_textarea_id: the comment_textarea_id is used to focus on the right comment form field after
	//the comment link is clicked
	//likes_count: the number of likes for the status
	//comments_count: the number of comments for this status
	//like_text: "Like" or "Unlike"
	//like_image: like or liked image
	//liked_users: the users that liked this status
	props: [
		'status_id', 
		'comment_textarea_id',
		'likes_count',
		'comments_count',
		'like_text',
		'like_image',
		'show',
		'liked_users'
	],
	methods: {
		toggleLike: function(event){
			var resource = null;

			if(this.like_text == 'Like'){
				resource = this.$resource('/likes/like{/id}');
				//send an ajax request
				resource.get({id: this.status_id}).then(
				function(response){
					console.log(response);
				},
				function(errors){
					console.log(errors);
				});

				//update the user interface
				this.like_image = this.likedImagePath;
				this.like_text = this.unlikeText;
				this.likes_count = parseInt(this.likes_count) + 1;
			}else{
				resource = this.$resource('/likes/unlike{/id}');
				//send an ajax request
				resource.get({id: this.status_id}).then(
				function(response){
					console.log(response);
				},
				function(errors){
					console.log(errors);
				});

				//update the user interface
				this.like_image = this.likeImagePath;
				this.like_text = this.likeText;
				this.likes_count = parseInt(this.likes_count) - 1;
			}
			
		},
		openPopup: function(event){
			this.$http.get('/likes/' + this.status_id).then((response) => {
            this.liked_users = response.json();
      	}, (errors) => {
          	console.log(errors);
      	});

		this.show = true;

		},
		activateComment: function(event){
			//set focus on the comment textarea
			$('#' + this.comment_textarea_id).focus();
		}
	}
});