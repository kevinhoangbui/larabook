<?php

$factory('App\User',
    [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => $faker->password
    ]);

$factory('App\Status',
    [
        'body' => $faker->paragraph,
        'user_id' => 'factory:App\User'
    ]);

$factory('App\Comment',
    [
        'status_id' =>'factory:App\Status',
        'user_id' => 'factory:App\User',
        'body' => $faker->paragraph
    ]);

$factory('App\Like',
    [
        'user_id' =>'factory:App\User',
        'likeable_id' => 'factory:App\Status',
        'likeable_type' => 'App\Status'
    ]);
