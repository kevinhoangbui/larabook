<?php
namespace Comments;

use Laracasts\TestDummy\Factory as TestDummy;
use App\Repositories\CommentsRepository;

class CommentsRepositoryTest extends \Codeception\TestCase\Test
{
    /**
     * @var \IntegrationTester
     */
    protected $tester;

    /**
     * The comments repository
     *
     * @var App\Repositories\CommentsRepository
     */
    protected $commentsRepo;
    

    protected function _before()
    {
        $this->commentsRepo = new CommentsRepository;
    }

    /** @test */
    public function a_user_can_make_a_comment_for_a_post()
    {
        //given I have a user and a status
        $user = TestDummy::create('App\User');
        $status = TestDummy::create('App\Status');
        //and some dummy text to comment
        $body = 'This is my comment!';

        //when I persist that comment
        $this->commentsRepo->leaveComment($user->id, $status->id, $body);

        //then I must see the record in the database
        $this->tester->seeRecord('comments', [
            'status_id' => $status->id,
            'user_id' => $user->id,
            'body' => $body
        ]);
    }
}
