<?php

use App\Repositories\StatusesRepository;
use App\Repositories\UserRepository;
use Laracasts\TestDummy\Factory as TestDummy;

class StatusesRepositoryTest extends \Codeception\TestCase\Test
{
    /**
     * @var \IntegrationTester
     */
    protected $tester;

    /**
     * the statuses repository
     *
     * @var StatusesRepository
     */
    protected $statusRepo;

    /**
     * the users repository
     *
     * @var StatusesRepository
     */
    protected $userRepo;

    protected function _before()
    {
        $this->statusRepo = new StatusesRepository;
        $this->userRepo = new UserRepository;
    }

    protected function _after()
    {
    }

    /** @test */
    public function it_gets_all_statuses_for_a_user()
    {
        //given I have two users
        $users = TestDummy::times(2)->create('App\User');

        //and statuses for both of them
        TestDummy::times(2)->create('App\Status', [
            'user_id' => $users[0]->id,
            'body' => 'User 1 status'
        ]);
        TestDummy::times(2)->create('App\Status', [
            'user_id' => $users[1]->id,
            'body' => 'User 2 status'
        ]);

        //when I get statuses for one user
        $statusesForUser = $this->statusRepo->getStatusesForUser($users[0]);

        //Then I should receive only the relevant ones
        $this->assertCount(2, $statusesForUser);
        $this->assertEquals('User 1 status', $statusesForUser[0]->body);
        $this->assertEquals('User 1 status', $statusesForUser[1]->body);
    }

    /** @test */
    public function it_saves_a_status_for_a_user()
    {
        //Given I have an unsaved status
        $status = TestDummy::create('App\Status', [
            'body' => 'My status'
        ]);

        //And an existing user
        $user = TestDummy::create('App\User');
        //When I try to persist this status
        $this->statusRepo->save($status, $user->id);
        //Then It should be saved
        $this->tester->seeRecord('statuses', [
            'body' => 'My status',
            'user_id' => $user->id
        ]);
    }

    /** @test */
    public function it_gets_the_feed_for_a_user()
    {
        //Given I have a number of users
        $users = TestDummy::times(4)->create('App\User');

        //and the first user follow the others
        $this->userRepo->follow($users[1]->id, $users[0]);
        $this->userRepo->follow($users[2]->id, $users[0]);
        $this->userRepo->follow($users[3]->id, $users[0]);

        //and each of them have a couple of statuses
        TestDummy::times(2)->create('App\Status', ['user_id' => $users[0]->id]);
        TestDummy::times(2)->create('App\Status', ['user_id' => $users[1]->id]);
        TestDummy::times(2)->create('App\Status', ['user_id' => $users[2]->id]);
        TestDummy::times(2)->create('App\Status', ['user_id' => $users[3]->id]);

        //when the first user get their feed
        $statusesForFirstUser = $this->statusRepo->getFeedForUser($users[0]);

        //then he should see the status from users he follows and himself
        $this->assertCount(8, $statusesForFirstUser);
    }
}
