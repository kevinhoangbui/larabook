<?php
use App\status;
use App\User;
use Codeception\TestCase\Test;
use Laracasts\TestDummy\Factory as TestDummy;

class LikesTest extends TestCase
{

    protected $status;

    public function setUp()
    {
        parent::setUp();

        //given I have a status
        $this->status = TestDummy::create('App\Status');
        $this->signIn();
    }

    /** @test */
    function a_user_can_like_a_status()
    {
        //when they like a status
        $this->status->like();
        
        //then we should see the evidence in the database, and the status should be liked
        $this->seeInDatabase('likes', [
            'user_id' => $this->user->id,
            'likeable_id' => $this->status->id,
            'likeable_type' => get_class($this->status)
        ]);

        $this->assertTrue($this->status->isLiked());
    }

    /** @test */
    function a_user_can_unlike_a_status()
    {
        //when they like a status
        $this->status->like();
        //and unlike it
        $this->status->unlike();
        
        //then we should see the evidence in the database, and the status should be liked
        $this->notSeeInDatabase('likes', [
            'user_id' => $this->user->id,
            'likeable_id' => $this->status->id,
            'likeable_type' => get_class($this->status)
        ]);

        $this->assertFalse($this->status->isLiked());
    }

    /** @test */
    function a_user_may_toggle_a_status_like_status()
    {
        //when they like a status
        $this->status->toggle();
        
        //then we should see the evidence in the database, and the status should be liked
        $this->assertTrue($this->status->isLiked());

        //then they unlike a status
        $this->status->toggle();

        //then the status should be unliked
        $this->assertFalse($this->status->isLiked());
    }

    /** @test */
    function a_status_knows_how_many_likes_it_has()
    {
        $this->status->toggle();
        
        
        $this->assertEquals(1, $this->status->likesCount);
    }

    /** @test */
    function a_user_is_able_to_see_all_the_users_that_liked_the_status()
    {
        //given the status has four likes
        TestDummy::times(4)->create('App\Like', [
            'likeable_id' => $this->status->id
        ]);

        $likedUsers = $this->status->likedUsers()->get();

        $this->assertEquals(4, $likedUsers->count());
    }

    private function signIn($user = null)
    {

        if (!$user) {
            $user = TestDummy::create(App\User::class);
        }

        $this->user = $user;

        $this->actingAs($this->user);
        
        $this->actingAs($user);

        return $this;
    }
}
