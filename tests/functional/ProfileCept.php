<?php
$I = new FunctionalTester($scenario);
$I->am('a Larabook member');
$I->wantTo('view my profile');

$I->signIn();
$I->click('Your Profile');

$I->postAStatus('My post on my profile page!');
$I->seeCurrentUrlEquals('/abby.baz@example.com');
$I->see('My post on my profile page!');
