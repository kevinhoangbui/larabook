<?php
$I = new FunctionalTester($scenario);
$I->am('a Larabook member');
$I->wantTo('login to my Larabook account.');

$I->haveAnAccount([
    'email' => 'john.doe@example.com',
    'password' => 'password'
]);

$I->amOnPage('/login');
$I->fillField('email', 'john.doe@example.com');
$I->fillField('password', 'password');
$I->click('Sign In');

$I->seeInCurrentUrl('/statuses');
$I->see('Welcome back!');
