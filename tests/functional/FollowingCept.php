<?php
$I = new FunctionalTester($scenario);
$I->am('a Larabook user.');
$I->wantTo('follow other Larabook users.');

//setup
$I->haveAnAccount(['email' => 'amberrose@example.com', 'name' => 'Amber Rose']);
$I->signIn();

//expectations
$I->amOnPage('/amberrose@example.com');
$I->click('Follow Amber Rose');
$I->seeCurrentUrlEquals('/amberrose@example.com');

$I->click('Unfollow Amber Rose');
$I->seeCurrentUrlEquals('/amberrose@example.com');
$I->see('Follow Amber Rose');
