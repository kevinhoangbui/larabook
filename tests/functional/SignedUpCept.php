<?php
$I = new FunctionalTester($scenario);
$I->am('a guest');
$I->wantTo('Sign up for a new larabook account');

$I->amOnPage('/');
$I->click('Sign Up');
$I->seeCurrentUrlEquals('/register');

$I->fillField('Name:', 'Ernest C. Belle');
$I->fillField('Email:', 'ernest.belle@example.com');
$I->fillField('Password:', 'password');
$I->fillField('Password Confirmation:', 'password');
$I->click('Sign Up');

$I->seeCurrentUrlEquals('/');
$I->see('Glad to have you as a new Larabook member!');

$I->seeRecord('users', [
        'name' => 'Ernest C. Belle',
        'email' => 'ernest.belle@example.com'
    ]);
