<?php
$I = new FunctionalTester($scenario);
$I->am('a Larabook member');
$I->wantTo('review all users who registered for Larabook');

$I->haveAnAccount(['name' => 'Bar']);
$I->haveAnAccount(['name' => 'Baz']);

$I->amOnPage('users');

$I->see('Bar');
$I->see('Baz');
