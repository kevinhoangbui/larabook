@extends('layouts.default')

@section('content')
	<div class="row">
		<div class="col-md-6 col-md-offset-3" id="statuses-column">
			
			@include('statuses.partials.publish-status-form')
			@foreach($statuses as $status)
				@include('statuses.partials.status')
			@endforeach

		<modal :show.sync="showModal" :liked_users.sync="liked_users">
    		<h3 slot="header">Likes</h3>
  		</modal>
		</div>
	</div>

	@include('statuses.vue-templates.likes-and-comments-template')
	@include('statuses.vue-templates.modal-template')
@stop

@section('custom-script')
	<script src="js/publish-status.js"></script>
	<script src="js/likes-and-comments-component.js"></script>
	<script src="js/modal-component.js"></script>
	<script src="js/initialize-vue.js"></script>
@stop