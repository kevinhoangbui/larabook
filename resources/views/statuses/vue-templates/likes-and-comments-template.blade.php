<!-- This is the Vue template  for likes-and-comments panel -->
<template id="likes-and-comments-template">
	<div class="panel-body">
		<a href="#" v-on:click.prevent="toggleLike"><img v-bind:src="like_image"/> @{{like_text}}</a>
		<a href="#" v-on:click.prevent="activateComment"><img v-bind:src="commentImagePath"/> Comment</a>
		<a href="#" @click="openPopup">
			@{{likes_count}} <img v-bind:src="likedImagePath"/>
		</a>
		@{{comments_count}} <img v-bind:src="commentsImagePath"/>
	</div>
</template>