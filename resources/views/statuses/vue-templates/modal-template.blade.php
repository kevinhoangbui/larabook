<template id="modal-template">
  <div class="modal-mask" v-show="show" transition="modal">
    <div class="modal-wrapper">
      <div class="modal-container">

        <div class="modal-header">
          <slot name="header">
            default header
          </slot>
        </div>
        
        <div class="modal-body">
          <slot name="body">
            <div class="media" v-for="user in liked_users">
              <div class="media-left media-middle">
                <img class="media-object" src="@{{user.avatar}}" alt="@{{user.name}}">
              </div>
              <div class="media-body">
                <h4>@{{user.name}}</h4>
              </div>
            </div>
          </slot>
        </div>

        <div class="modal-footer">
          <slot name="footer">
            <button class="modal-default-button"
              @click="show = false">
              OK
            </button>
          </slot>
        </div>
      </div>
    </div>
  </div>
</template>