<article class="media status-media">
	<div class="pull-left">
		<img class="media-object" src="{{$status->user->avatar}}" alt="{{$status->user->name}}">
	</div>
	<div class="media-body">
		<h4 class="media-heading">{{$status->user->name}}</h4>
		<p>{{$status->present()->created_at()}}</p>
		{{$status->body}}
	</div>
</article>

@if($signedIn)

<div class="panel panel-default">
	<likes-and-comments status_id="{{$status->id}}" 
						comment_textarea_id="{{'comment-for-status-' . $status->id}}"
						likes_count="{{$status->likes_count}}"
						comments_count="{{$status->comments_count}}"
						like_image="{{$status->like_image}}"
						like_text="{{$status->like_text}}"
						:show.sync="showModal"
						:liked_users.sync="liked_users">
	</likes-and-comments>
	{!!Form::open(['route' => 'comment_path', 'class' => 'comments__create-form'])!!}
	{!!Form::hidden('status_id', $status->id)!!}
	{!!Form::textarea('body', null, ['class'=>'form-control', 
									'rows'=>1, 
									'placeholder' => 'Leave a comment...',
									'id' => 'comment-for-status-' . $status->id])!!}
	{!!Form::close()!!}
	@unless($status->comments->isEmpty())
		<div>
			@foreach ($status->comments as $comment)
			@include('statuses.partials.comment')
			@endforeach
		</div>
	@endunless
</div>
@endif

