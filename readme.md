# Larabook

Larabook is a social networking site developed with Laravel, a popular PHP framework. This site is developed for Laravel developers to exchange knowledge. New features will be constantly added. Please visit http://104.131.106.143/ to see the website.

You may login with the following credentials: Email: Naomi.Bergnaum@example.com, password: password. You may register to become a member too.
