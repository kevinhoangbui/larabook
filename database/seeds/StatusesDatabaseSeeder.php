<?php

use Illuminate\Database\Seeder;
use App\User;

class StatusesDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$users = User::pluck('id');

        factory('App\Status', 200)->create();
    }
}
