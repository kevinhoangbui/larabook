<?php

namespace App\Repositories;

use App\Comment;
use App\User;

class CommentsRepository
{
    
    /**
     * Save a comment to the database
     *
     * @param  integer $userId the user who created the comment
     * @param  integer $statusId
     * @param  string $body the content of the comment
     * @return App\Comment
     */
    public function leaveComment($userId, $statusId, $body)
    {
        $comment = Comment::leave($body, $statusId);

        User::findOrFail($userId)->comments()->save($comment);

        return $comment;
    }
}
