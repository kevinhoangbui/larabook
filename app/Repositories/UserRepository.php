<?php

namespace App\Repositories;

use App\User;

class UserRepository
{

    /**
     * Register a new user
     *
     * @param  App\User $user
     * @return void
     */
    public function save(User $user)
    {
        return $user->save();
    }

    /**
     * Retrieve the paginated list of all users
     *
     * @param  integer $howMany number of users displayed per page
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function getPaginated($howMany = 20)
    {
        return User::orderBy('name', 'asc')->paginate($howMany);
    }

    /**
     * Find a user by their email
     *
     * @param  string $email
     * @return App\User
     */
    public function findByEmail($email)
    {
        return User::with('statuses')->whereEmail($email)->first();
    }

    /**
     * Find a user by id
     * @param  integer $id
     * @return App\User
     */
    public function findById($id)
    {
        return User::findOrFail($id);
    }

    /**
     * Follow a specific user
     *
     * @param  integer  $userIdToFollow the id of the user to follow
     * @param  App\User $user the user that will follow
     * @return void
     */
    public function follow($userIdToFollow, User $user)
    {
        return $user->follows()->attach($userIdToFollow);
    }

    /**
     * Unfollow a specific user
     *
     * @param  integer  $userIdToUnfollow the id of the user to unfollow
     * @param  App\User $user the user that will unfollow
     * @return void
     */
    public function unfollow($userIdToUnfollow, User $user)
    {
        return $user->follows()->detach($userIdToUnfollow);
    }
}
