<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Repositories\CommentsRepository;

class CommentsController extends Controller
{
    protected $commentsRepo;

    public function __construct(CommentsRepository $commentsRepo)
    {
        $this->commentsRepo = $commentsRepo;
    }


    public function store(Request $request)
    {
        //fetch the input
        $input = array_add($request->all(), 'user_id', \Auth::id());

        //execute the command
        $comment = $this->commentsRepo->leaveComment($input['user_id'], $input['status_id'], $input['body']);

        //go back
        return redirect()->back();
    }
}
