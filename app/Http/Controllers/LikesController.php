<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Status;

class LikesController extends Controller
{

    /**
     * Insert a like to the database
     *
     * @param  int $statusId
     * @return string
     */
    public function like($statusId)
    {
        $status = Status::findOrFail($statusId);
        $status->like();
        return 'You have like the status id ' . $statusId;
    }

    /**
     * Remove a like from the database
     *
     * @param  int $statusId
     * @return string
     */
    public function unlike($statusId)
    {
        $status = Status::findOrFail($statusId);
        $status->unlike();
        return 'You have unlike the status id ' . $statusId;
    }

    /**
     * Retrieve all users that like a status
     *
     * @param  integer $statusId
     * @return JSON
     */
    public function index($statusId)
    {
        $status = Status::findOrFail($statusId);
        $likedUsers = $status->likedUsers()->get();

        return $likedUsers;
    }
}
