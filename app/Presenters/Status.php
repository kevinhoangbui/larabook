<?php
namespace App\Presenters;

class Status extends Presenter
{

    /**
     * Display time time since the status is posted
     *
     * @return string
     */
    public function created_at()
    {
        return $this->entity->created_at->diffForHumans();
    }
}
