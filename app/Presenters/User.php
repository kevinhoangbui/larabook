<?php

namespace App\Presenters;

class User extends Presenter
{

    /**
     * Show the user' avatar from gravatar.com
     *
     * @param  integer $size the size of the image (avatar)
     * @return string the url to the avatar
     */
    public function gravatar($size = 40)
    {
        $email = md5($this->email);
        return "//www.gravatar.com/avatar/{$email}?s={$size}";
    }

    /**
     * Display the number of followers for this user
     * @return string
     */
    public function followerCount()
    {
        $count = $this->entity->followers()->count();
        $plural = str_plural('Follower', $count);

        return "{$count} {$plural}";
    }

    /**
     * Display the number of statuses for this user
     * @return string
     */
    public function statusCount()
    {
        $count = $this->entity->statuses()->count();
        $plural = str_plural('Status', $count);

        return "{$count} {$plural}";
    }
}
