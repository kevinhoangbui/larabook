<?php
namespace App;

use Auth;

trait Likeability
{

    /**
     * the path to the like.jpg image
     */
    protected $likeImagePath = 'images/icons/like.jpg';

    /**
     * the path to the liked.jpg image
     */
    protected $likedImagePath = 'images/icons/liked.jpg';

    /**
     * Represent the porlymorphic relationship between App\Status and App\Like
     *
     * @return Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable');
    }

    /**
     * Persist a new like to the database from the authenticated user
     *
     * @return void
     */
    public function like()
    {
        $like = new Like(['user_id'=>Auth::id()]);

        $this->likes()->save($like);
    }

    /**
     * Remove the like for this status from the authenticated user
     *
     * @return void
     */
    public function unlike()
    {
        $this->likes()->where('user_id', Auth::id())->delete();
    }

    /**
     * Check whether this status is liked by the authenticated user
     *
     * @return boolean
     */
    public function isLiked()
    {
        return !! $this->likes()
                    ->where('user_id', Auth::id())
                    ->count();
    }

    /**
     * Toggle the like/unlike function
     *
     * @return void
     */
    public function toggle()
    {
        if ($this->isLiked()) {
            $this->unlike();
        } else {
            $this->like();
        }
    }

    /**
     * Retrieve the number of likes for this status
     *
     * @return int
     */
    public function getLikesCountAttribute()
    {
        return $this->likes->count();
    }

    /**
     * Display the like/liked image for this status.
     * If the user liked the status, the liked image will be displayed.
     * On the other hand, the like image will be displayed.
     *
     * @return string the path to the like/liked image
     */
    public function getLikeImageAttribute()
    {
        return $this->isLiked() ? $this->likedImagePath : $this->likeImagePath;
    }

    /**
     * Display the text of the like/liked button for this status
     * If the user liked the status, The text will be "Unlike".
     * On the other hand, the text will be "Like".
     *
     * @return string
     */
    public function getLikeTextAttribute()
    {
        return $this->isLiked() ? 'Unlike' : 'Like';
    }

    /**
     * Retrieve the list of users that liked this status
     *
     * @return Illuminate\Database\Relations\BelongsToMany
     */
    public function likedUsers()
    {
        return $this->belongsToMany('App\User', 'likes', 'likeable_id', 'user_id');
    }
}
